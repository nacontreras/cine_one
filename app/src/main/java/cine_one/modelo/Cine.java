package cine_one.modelo;

import java.util.List;

public class Cine {
    public static final int CAPACIDAD = 80;
    private String nombre;
    private Venta venta;

    private List<Programacion> programaciones;
    private List<Funcion> funciones;
    private CatalogoProducto catalogoProductos;

    public Cine(String nombre, CatalogoProducto catalogoProductos) {
        this.nombre = nombre;
        this.catalogoProductos = new CatalogoProducto(catalogoProductos);
    }

    public CatalogoProducto getCatalogoProductos() {
        return catalogoProductos;
    }

    public List<Funcion> getFunciones() {
        return funciones;
    }

    public List<Programacion> getProgramaciones() {
        return programaciones;
    }

}
