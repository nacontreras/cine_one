/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cine_one.modelo;

/**
 *
 * @author Nelson
 */
public class EspecificacionProducto implements IVendible {
    private String codigo;
    private String descripcion;
    private Double precio;

    public EspecificacionProducto(String codigo, String descripcion, Double precio) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public Double getPrecio() {
        // TODO Auto-generated method stub
        // throw new UnsupportedOperationException("Unimplemented method 'getPrecio'");
        return this.precio;
    }

    @Override
    public String getDetalle() {
        // TODO Auto-generated method stub
        // throw new UnsupportedOperationException("Unimplemented method 'getDetalle'");
        return "Codigo: " + this.codigo + "Descripcion: " + this.descripcion;
    }

}
