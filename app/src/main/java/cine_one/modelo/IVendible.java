/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cine_one.modelo;

/**
 *
 * @author Nelson
 */
public interface IVendible {
    public Double getPrecio();

    public String getDetalle();
}
