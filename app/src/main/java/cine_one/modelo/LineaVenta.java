package cine_one.modelo;

import java.time.LocalDate;
import java.util.List;

public class LineaVenta {
    private Integer cantidad;
    private Double precio;
    private double subtotal;
    private Asiento asientoPedido;
    private Espectador espectador;

    public LineaVenta(Integer cantidad, Asiento asientoPedido) {
        this.cantidad = cantidad;
        this.asientoPedido = asientoPedido;
        this.crearEspectador();
    }

    private void crearEspectador() {
          this.espectador = new Espectador("Juan Alberto", 25, 5820.00);
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public Double calcularTotalLinea() {
        Double totalLinea = 0.00;
        totalLinea = this.getCantidad() * EspecificacionEntrada.PRECIO;
        return totalLinea;
    }

    public Asiento getAsientoPedido() {
        return asientoPedido;
    }

    public Espectador getEspectador() {
        return espectador;
    }

    
}
