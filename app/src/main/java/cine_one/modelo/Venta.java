package cine_one.modelo;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import cine_one.persistencia.RegistroVenta;

public class Venta {
    private LocalDateTime fechaHoraVenta;
    private Funcion funcion;
    private List<LineaVenta> lineasVenta;
    private LineaVenta lineaVenta;
    private Boolean estadoVenta;
    private Persona espectador;
    public static Integer numeroTicket = 0;

    public Venta() {
    };

    public Venta(Funcion funcion) {
        // this.cliente=cliente;
        this.funcion = funcion;
        this.lineasVenta = new ArrayList<LineaVenta>();
        numeroTicket++;
        this.fechaHoraVenta = LocalDateTime.now();
        this.estadoVenta = true;
    }

    public void vender(Integer cantidad, Integer nroAsiento) throws AsientoOcupadoException {
        Asiento asientoPedido = this.funcion.buscarAsiento(nroAsiento);
        if (!asientoPedido.getOcupado()) {
            CrearLineaVenta(cantidad, asientoPedido);
            this.funcion.actualizarEstadoAsiento(true, asientoPedido);
            generarEntrada(this.lineaVenta);
        } else {
            throw new AsientoOcupadoException("El asiento ya fue vendido");
        }
    }

    public void CrearLineaVenta(Integer cantidad, Asiento asientoPedido) {
        this.lineaVenta = new LineaVenta(cantidad, asientoPedido);
        lineasVenta.add(lineaVenta);
    }

    public Double obtenerTotalVenta() {
        Double totalVenta = 0.0;
        for (LineaVenta lineaVenta : this.lineasVenta) {
            totalVenta += lineaVenta.calcularTotalLinea();
        }

        return totalVenta;
    }

    public void registrarVenta() {
        RegistroVenta registroVenta = new RegistroVenta();
        registroVenta.agregarVenta(this);
    }

    public void generarEntrada(LineaVenta lineaVenta) {
        Entrada unaEntrada = new Entrada(Venta.numeroTicket, fechaHoraVenta.toLocalDate(), funcion,
                EspecificacionEntrada.PRECIO, lineaVenta.getEspectador(), lineaVenta.getAsientoPedido());
        this.funcion.agregarEntrada(unaEntrada);
    }

    protected void imprimirVenta() {
        ImprimeVenta imprimir = new ImprimeVenta(this);
        imprimir.imprimirVenta();
    }

    protected void imprimirEntrada() {
        ImprimeEntrada imprimirEntrada = new ImprimeEntrada(this);
        imprimirEntrada.imprimirEntradas();
    }

    public LocalDateTime getFechaHoraVenta() {
        return fechaHoraVenta;
    }

    public Funcion getFuncion() {
        return funcion;
    }

    public List<LineaVenta> getLineasVenta() {
        return lineasVenta;
    }

    public static Integer getNumeroTicket() {
        return numeroTicket;
    }

}
