package cine_one.modelo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Funcion {
    private Integer numeroFuncion;
    private LocalDate fechaFuncion;
    private LocalTime horaFuncion;
    private Pelicula pelicula;
    private List<Entrada> entradas;
    private List<Asiento> asientosSala;

    public Funcion(Integer numeroFuncion, LocalDate fechaFuncion, LocalTime horaFuncion, Pelicula pelicula) {
        this.numeroFuncion = numeroFuncion;
        this.fechaFuncion = fechaFuncion;
        this.horaFuncion = horaFuncion;
        this.pelicula = pelicula;
        this.entradas = new ArrayList<Entrada>();
        this.inicializarAsientosSala(Cine.CAPACIDAD);
    }

    public Integer getNumeroFuncion() {
        return numeroFuncion;
    }

    public void setNumeroFuncion(Integer numeroFuncion) {
        this.numeroFuncion = numeroFuncion;
    }

    public LocalDate getFechaFuncion() {
        return fechaFuncion;
    }

    public void setFechaFuncion(LocalDate fechaFuncion) {
        this.fechaFuncion = fechaFuncion;
    }

    public LocalTime getHoraFuncion() {
        return horaFuncion;
    }

    public void setHoraFuncion(LocalTime horaFuncion) {
        this.horaFuncion = horaFuncion;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public List<Entrada> getEntradaFuncion() {
        return entradas;
    }

    public void setEntradaFuncion(List<Entrada> entradaFuncion) {
        this.entradas = entradaFuncion;
    }

    public List<Entrada> getEntradas() {
        return entradas;
    }

    public void setEntradas(List<Entrada> entradas) {
        this.entradas = entradas;
    }

    public List<Asiento> getAsientosSala() {
        return asientosSala;
    }

    protected void inicializarAsientosSala(int capacidad) {
        asientosSala = new ArrayList<Asiento>();
        for (int i = 0; i < capacidad; i++) {
            Asiento unAsiento = new Asiento(i);
            unAsiento.setOcupado(false);
            asientosSala.add(unAsiento);
        }
    }

    protected boolean validarAsientoDisponible(Asiento unAsiento) {
        return this.asientosSala.get(unAsiento.getNumero() - 1).getOcupado();
    }

    public Asiento buscarAsiento(Integer nroAsiento) {
        Asiento unAsiento = null;    
        for (Asiento asiento : asientosSala) {
                if (asiento.getNumero() == nroAsiento){
                    unAsiento= asiento;
                    break;
                }
            }
       // return this.getAsientosSala().get(nroAsiento);
        return unAsiento;
    }

    public void actualizarEstadoAsiento(boolean b, Asiento asiento) {
       Asiento unAsiento = this.buscarAsiento(asiento.getNumero());
        if (!unAsiento.getOcupado()){
            unAsiento.modificarEstadoAsiento(b);
        } else {
            new AsientoOcupadoException();
        }
/*         if (!this.asientosSala.get(asiento.getNumero()).getOcupado()) {
            this.asientosSala.get(asiento.getNumero()).setOcupado(b);

        } else {
            new AsientoOcupadoException();
        }
 */
    }

    public void agregarEntrada(Entrada unaEntrada) {
        this.entradas.add(unaEntrada);
    }

}
