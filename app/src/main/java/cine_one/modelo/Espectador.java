package cine_one.modelo;

public class Espectador extends Persona {
    private Integer edad;
    private Double dinero;

    public Espectador(String nombre, Integer edad, Double dinero) {
        super(nombre);
        this.edad = edad;
        this.dinero = dinero;
    }

    @Override
    public String toString() {
        return "Espectador [nombre:" + super.getNombre() + ", edad:" + edad + ", dinero:" + dinero + "]";
    }

}
