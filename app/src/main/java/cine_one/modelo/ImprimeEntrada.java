package cine_one.modelo;


public class ImprimeEntrada {
    private Entrada entrada;
    private Venta venta;

    public ImprimeEntrada(Venta venta) {
        this.venta = venta;
    }

    public void imprimirEntradas(){
        for (Entrada unaEntrada : venta.getFuncion().getEntradaFuncion()) {
            unaEntrada.toString();
        }
    }
}
