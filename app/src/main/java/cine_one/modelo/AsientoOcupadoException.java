package cine_one.modelo;

public class AsientoOcupadoException extends Exception {
    AsientoOcupadoException() {
        super();
    }

    AsientoOcupadoException(String mensaje) {
        super(mensaje);
    }

    public String ToString() {
        return ("Excepción producida" + this.getClass().getName() + "\n" + "Mensaje: " + this.getMessage());
    }
}
