package cine_one.modelo;

import java.time.LocalDate;

public class Entrada {
    private Integer nroTicket;
    private LocalDate fechaVenta;
    private Funcion funcion;
    private Double precio;
    private Asiento asiento;
    private Espectador espectador;

    public Entrada(Integer nroTicket, LocalDate fechaVenta, Funcion funcion, Double precio, Espectador espectador, Asiento asiento) {
        this.nroTicket = nroTicket;
        this.fechaVenta = fechaVenta;
        this.funcion = funcion;
        this.precio = precio;
        this.espectador = espectador;
        this.asiento = asiento;
    }
    
    public Integer getNroTicket() {
        return nroTicket;
    }
    public void setNroTicket(Integer nroTicket) {
        this.nroTicket = nroTicket;
    }
    public LocalDate getFechaVenta() {
        return fechaVenta;
    }
    public void setFechaVenta(LocalDate fechaVenta) {
        this.fechaVenta = fechaVenta;
    }
    public Funcion getFuncion() {
        return funcion;
    }
    public void setFuncion(Funcion funcion) {
        this.funcion = funcion;
    }
    public Double getPrecio() {
        return precio;
    }
    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Entrada [nroTicket=" + nroTicket + ", fechaVenta=" + fechaVenta + ", funcion=" + funcion.getNumeroFuncion() + ", precio="
                + precio + ", asiento=" + asiento.getNumero() + ", espectador=" + espectador.getNombre() + "]";
    }
    
    
}
