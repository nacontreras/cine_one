package cine_one.modelo;

import java.util.List;

public class Pelicula{
    private String titulo;
    private String duracion;
    private String director;
    private List <Actor> protagonistas;
    private Genero genero;


    
    public Pelicula(String titulo, String duracion, Director director, List<Actor> protagonistas, Genero genero) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.director = director.getNombre();
        this.protagonistas = protagonistas;
        this.genero = genero;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getDuracion() {
        return duracion;
    }
    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }
    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }
    public List<Actor> getProtagonistas() {
        return protagonistas;
    }
    public void setProtagonistas(List<Actor> protagonistas) {
        this.protagonistas = protagonistas;
    }
    public Genero getGenero() {
        return genero;
    }
    public void setGenero(Genero genero) {
        this.genero = genero;
    }
    
    
}