package cine_one.modelo;

import java.util.List;

public class Asiento {
    private int numero;
    private Boolean ocupado;

    public Asiento(int numero) {
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Boolean getOcupado() {
        return ocupado;
    }

    public void setOcupado(Boolean ocupado) {
        this.ocupado = ocupado;
    }

    public Asiento asignarAsiento(List<Asiento> asientosFuncion, int numero) {
        Asiento asientoAsignado = null;
        if (numero > 0) {
            if (asientosFuncion.get(numero).ocupado == false) {
                asientoAsignado = asientosFuncion.get(numero);
            }
        }
        return asientoAsignado;
    }

    public Boolean estaOcupado(List<Asiento> asientosSala, int i) {
        return asientosSala.get(i - 1).getOcupado();
    }

    public void modificarEstadoAsiento(boolean b) {
        this.setOcupado(b);
    }
}
