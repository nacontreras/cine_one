package cine_one.axiliares;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public abstract class FormatoFechaHora {

    public static  LocalDate formatoFecha(String fechaString){
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fechaConFormat = LocalDate.parse(fechaString, formato);
        return fechaConFormat;
    }

    public static LocalTime formatoHora(String horaString){
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime horaConFormato = LocalTime.parse(horaString, formato);
        return horaConFormato;
    }

}
