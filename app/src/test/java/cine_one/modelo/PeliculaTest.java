package cine_one.modelo;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class PeliculaTest {
    @Test
    public void testListaDeProtagonistas() {
        List<Actor> actores = new ArrayList<Actor>();
        Actor mel = new Actor("Mel Gibson");
        Actor dany = new Actor("Danny Glover");
        Actor traci = new Actor("Traci Wolfe");
        actores.add(mel);
        actores.add(dany);
        actores.add(traci);

        Pelicula armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                Genero.ACCION);
        int actual = armaMortal.getProtagonistas().size();
        Object[] actorePelicula = armaMortal.getProtagonistas().toArray();
        Object[] actoresEsperados = actores.toArray();
        assertArrayEquals("Verificando protagonistas de las pelicula", actoresEsperados, actorePelicula);
        assertEquals("protagonistas", 3, actual);
    }
}
