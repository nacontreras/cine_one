package cine_one.modelo;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cine_one.axiliares.FormatoFechaHora;

public class FuncionTest {

    // private List<String> list;
    private List<Actor> actores;
    private Pelicula armaMortal;
    private Funcion unaFuncion;

    @Before
    public void init() {
        Actor mel = new Actor("Mel Gibson");
        Actor dany = new Actor("Danny Glover");
        Actor traci = new Actor("Traci Wolfe");

        actores = new ArrayList<Actor>(Arrays.asList(new Actor("Mel Gibson"), dany, traci));

        armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                Genero.ACCION);
        unaFuncion = new Funcion(1, FormatoFechaHora.formatoFecha("10/10/2022"),
                FormatoFechaHora.formatoHora("22:15"), armaMortal);
    }

    @Test
    public void testGenerarAsientosDisponiblesInicial() {
        // Actor mel = new Actor("Mel Gibson");
        // Actor dany = new Actor("Danny Glover");
        // Actor traci = new Actor("Traci Wolfe");
        // List <Actor> actores = new ArrayList <Actor>();
        // actores.add(mel);
        // actores.add(dany);
        // actores.add(traci);

        armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                Genero.ACCION);
        unaFuncion = new Funcion(1, FormatoFechaHora.formatoFecha("10/10/2022"),
                FormatoFechaHora.formatoHora("22:15"), armaMortal);

        assertEquals(80, unaFuncion.getAsientosSala().size());
    }

    @After
    public void teardown() {
        actores.clear();
    }

    @Test
    public void testValidarAsientosDisponibles() {
        unaFuncion.inicializarAsientosSala(80);
        unaFuncion.getAsientosSala();
    }
}
