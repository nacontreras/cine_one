package cine_one.modelo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cine_one.axiliares.FormatoFechaHora;

public class AsientoTest {
    @Test
    public void testIniciarAsientoSala() {

        Actor mel = new Actor("Mel Gibson");
        Actor dany = new Actor("Danny Glover");
        Actor traci = new Actor("Traci Wolfe");
        List<Actor> actores = new ArrayList<Actor>();
        actores.add(mel);
        actores.add(dany);
        actores.add(traci);

        Pelicula armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                Genero.ACCION);
        Funcion unaFuncion = new Funcion(1, FormatoFechaHora.formatoFecha("10/10/2022"),
                FormatoFechaHora.formatoHora("22:15"), armaMortal);

        assertEquals("asientos: ", 80, unaFuncion.getAsientosSala().size());

    }
}
