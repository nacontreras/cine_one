package cine_one.modelo;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cine_one.axiliares.FormatoFechaHora;

public class VentaTest {
        List<Actor> actores;
        Pelicula armaMortal;
        Funcion unaFuncion;
        Actor mel, dany, traci;

        @Before
        public void init() {
                Venta.numeroTicket = 0;
                mel = new Actor("Mel Gibson");
                dany = new Actor("Danny Glover");
                traci = new Actor("Traci Wolfe");

                actores = new ArrayList<Actor>(Arrays.asList(new Actor("Mel Gibson"), dany, traci));

                armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                                Genero.ACCION);
                unaFuncion = new Funcion(1, FormatoFechaHora.formatoFecha("10/10/2022"),
                                FormatoFechaHora.formatoHora("22:15"), armaMortal);
        }

        @Test
        public void testCuandoVendoUnaEntrada() throws AsientoOcupadoException {
                // Actor mel = new Actor("Mel Gibson");
                // Actor dany = new Actor("Danny Glover");
                // Actor traci = new Actor("Traci Wolfe");
                // List<Actor> actores = new ArrayList<Actor>();
                // actores.add(mel);
                // actores.add(dany);
                // actores.add(traci);
                // Pelicula armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un
                // director"), actores,
                // Genero.ACCION);
                // Funcion unaFuncion = new Funcion(1,
                // FormatoFechaHora.formatoFecha("16/10/2022"),
                // FormatoFechaHora.formatoHora("22:15"), armaMortal);

                Integer cantidad = 1;
                Venta venta = new Venta(unaFuncion);

                venta.vender(cantidad, 5);
                Double totalVentaEsperado = EspecificacionEntrada.PRECIO * cantidad;
                Double totalVenta = venta.obtenerTotalVenta();

                assertEquals("Total Vendido", totalVentaEsperado, totalVenta, 0);
        }

        @Test
        public void testCuandoVendoDosEntradas() throws AsientoOcupadoException {
                /*
                 * Actor mel = new Actor("Mel Gibson");
                 * Actor dany = new Actor("Danny Glover");
                 * Actor traci = new Actor("Traci Wolfe");
                 * List<Actor> actores = new ArrayList<Actor>();
                 * actores.add(mel);
                 * actores.add(dany);
                 * actores.add(traci);
                 * Pelicula armaMortal = new Pelicula("Arma Mortal", "120 min", new
                 * Director("Un director"), actores,
                 * Genero.ACCION);
                 * Funcion unaFuncion = new Funcion(1,
                 * FormatoFechaHora.formatoFecha("16/10/2022"),
                 * FormatoFechaHora.formatoHora("22:15"), armaMortal);
                 */
                Integer cantidad = 1;
                Integer cantidadvendida = 2;
                Venta venta = new Venta(unaFuncion);

                venta.vender(cantidad, 5);
                venta.vender(cantidad, 6);
                // venta.imprimirVenta();
                Double totalVentaEsperado = EspecificacionEntrada.PRECIO * cantidadvendida;
                Double totalVenta = venta.obtenerTotalVenta();
                assertEquals("Total Vendido", totalVentaEsperado, totalVenta, 0);
        }

        @Test(expected = AsientoOcupadoException.class)
        public void testCuandoVendoAsientoOcupado() throws AsientoOcupadoException {
                // Actor mel = new Actor("Mel Gibson");
                // Actor dany = new Actor("Danny Glover");
                // Actor traci = new Actor("Traci Wolfe");
                // List<Actor> actores = new ArrayList<Actor>();
                actores.add(mel);
                actores.add(dany);
                actores.add(traci);
                Pelicula armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                                Genero.ACCION);
                Funcion unaFuncion = new Funcion(1, FormatoFechaHora.formatoFecha("16/10/2022"),
                                FormatoFechaHora.formatoHora("22:15"), armaMortal);

                Integer cantidad = 1;
                Integer cantidadvendida = 2;
                Venta venta = new Venta(unaFuncion);

                venta.vender(cantidad, 5);
                venta.vender(cantidad, 5);

                Double totalVentaEsperado = EspecificacionEntrada.PRECIO * cantidadvendida;

                Double totalVenta = venta.obtenerTotalVenta();

                assertEquals("Total Vendido", totalVentaEsperado, totalVenta, 0);

        }

        @Test
        public void alIniciarUnaVentaEntoncesElNumerodeTicketSeIncrementa() {
                Actor mel = new Actor("Mel Gibson");
                List<Actor> actores = new ArrayList<Actor>();
                actores.add(mel);
                Pelicula armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                                Genero.ACCION);
                Funcion unaFuncion = new Funcion(1, FormatoFechaHora.formatoFecha("16/10/2022"),
                                FormatoFechaHora.formatoHora("22:15"), armaMortal);
                Integer nroTicketEsperado = 2;
                Venta venta = new Venta(unaFuncion);
                Venta venta1 = new Venta(unaFuncion);

                assertEquals(nroTicketEsperado, Venta.numeroTicket);
        }

        @After
        public void teardown() {
                Venta.numeroTicket = 0;
        }
}
