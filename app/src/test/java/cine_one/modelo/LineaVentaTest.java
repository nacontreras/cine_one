package cine_one.modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cine_one.axiliares.FormatoFechaHora;

public class LineaVentaTest {

    private List<Actor> actores;
    private Pelicula armaMortal;
    private Funcion unaFuncion;

    @Before
    public void init() {
        Actor mel = new Actor("Mel Gibson");
        Actor dany = new Actor("Danny Glover");
        Actor traci = new Actor("Traci Wolfe");

        actores = new ArrayList<Actor>(Arrays.asList(new Actor("Mel Gibson"), dany, traci));

        armaMortal = new Pelicula("Arma Mortal", "120 min", new Director("Un director"), actores,
                Genero.ACCION);
        unaFuncion = new Funcion(1, FormatoFechaHora.formatoFecha("10/10/2022"),
                FormatoFechaHora.formatoHora("22:15"), armaMortal);
    }

    /*
     * @Test
     * public void testAsientoSolicitadoEstaDisponible() {
     * List<Asiento> pepe = unaFuncion.getAsientosSala();
     * LineaVenta lineaventa = new LineaVenta(1, pepe);
     * Boolean estadoAsiento = lineaventa.validarAsientoDisponible(new Asiento(1));
     * assertFalse("Asiento disponible", estadoAsiento);
     * }
     */

    /*
     * @Test
     * public void testAsientoSolicitadoEstaOcupado() {
     * List<Asiento> unAsiento = unaFuncion.getAsientosSala();
     * unAsiento.get(10).setOcupado(true);
     * LineaVenta lineaventa = new LineaVenta(1, unAsiento);
     * Boolean estadoAsiento = lineaventa.validarAsientoDisponible(new Asiento(11));
     * assertTrue("Asiento No disponible", estadoAsiento);
     * }
     */

    /*
     * @Test
     * public void testCuandoVendoUnaEntrada() {
     * List<Asiento> unAsiento = unaFuncion.getAsientosDisponibles();
     * Double totalVentaEsperado = EspecificacionEntrada.PRECIO * 1;
     * LineaVenta lineaventa = new LineaVenta(1, unAsiento);
     * Double totalLinea = lineaventa.calcularTotalLinea();
     * assertEquals(totalVentaEsperado, totalLinea, 0);
     * }
     */

    /*
     * @Test
     * public void TestcuandoVendoDosEntrada() {
     * List<Asiento> asientos = unaFuncion.getAsientosDisponibles();
     * Double totalVentaEsperado = EspecificacionEntrada.PRECIO * 2;
     * LineaVenta lineaventa = new LineaVenta(2, asientos);
     * Double totalLinea = lineaventa.calcularTotalLinea();
     * assertEquals(totalVentaEsperado, totalLinea, 0);
     * }
     */

    @Test
    public void TestRegistrarEntraVendida() {

    }

}
